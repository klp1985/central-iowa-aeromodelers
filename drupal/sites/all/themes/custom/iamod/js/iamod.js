(function ($) {
    
    console.log('loaded...');
    
    Drupal.behaviors.slideshow = {
        attach: function (context, setting) {
            $('.flexslider', context).once('slideshow', function() {
                var $el = $(this);
                $el.flexslider({
                    animation: "slide"
                });
            });                       
        }
    };
    
}(jQuery));