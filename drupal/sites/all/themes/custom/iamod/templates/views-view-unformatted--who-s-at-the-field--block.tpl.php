<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */

?>

<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
    <?php print $row; ?>
  <hr />
  </div>
<?php endforeach; ?>

<p><a href = "/section/work/ia_aero_mod/drupal/node/add/fly-time" style = "text-align: right;">Add Your Fly Time</a></p>
