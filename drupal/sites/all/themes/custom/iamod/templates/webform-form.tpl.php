<?php

/**
 * @file
 * Customize the display of a complete webform.
 *
 * This file may be renamed "webform-form-[nid].tpl.php" to target a specific
 * webform on your site. Or you can leave it "webform-form.tpl.php" to affect
 * all webforms on your site.
 *
 * Available variables:
 * - $form: The complete form array.
 * - $nid: The node ID of the Webform.
 *
 * The $form array contains two main pieces:
 * - $form['submitted']: The main content of the user-created form.
 * - $form['details']: Internal information stored by Webform.
 *
 * If a preview is enabled, these keys will be available on the preview page:
 * - $form['preview_message']: The preview message renderable.
 * - $form['preview']: A renderable representing the entire submission preview.
 */
?>

<div class = "row">
  <div class = "col-sm-12 col-md-6">
    <h4>To become a member, fill out the form below:</h4>
    <p class = "border-line"></p>
<?php
  // Print out the progress bar at the top of the page
  print drupal_render($form['progressbar']);

  // Print out the preview message if on the preview page.
  if (isset($form['preview_message'])) {
    print '<div class="messages warning">';
    print drupal_render($form['preview_message']);
    print '</div>';
  }

  // Print out the main part of the form.
  // Feel free to break this up and move the pieces within the array.
  print drupal_render($form['submitted']);

  // Always print out the entire $form. This renders the remaining pieces of the
  // form that haven't yet been rendered above (buttons, hidden elements, etc).
  print drupal_render_children($form);?>

  </div>

  <div class = "col-sm-12 col-md-6">
    <h3>Annual Dues</h3>

												<p class = "border-line"></p>
                        <div class = "member-table-container">
													<div class = "row">
															<div class = "col-sm-4">
																	<h5 class = "member-table">Open Member</h5>
                              </div>
                              <div class = "col-sm-4">
																	<h5 class = "member-table">Student Member</h5>
                              </div>
                              <div class = "col-sm-4">
																	<h5 class = "member-table">Family Membership</h5>
															</div>
                          </div>

                          <div style = "border: 1px solid #820000; margin: 0; padding: 0;"></div>

															<div class = "row">
                                <div class = "col-sm-4">
																	<h4 class = "member-table">$70</h4>
                                </div>
                                <div class = "col-sm-4">
																	<h4 class = "member-table">$35</h4>
                                </div>
                                <div class = "col-sm-4">
																	<h4 class = "member-table">$70</h4>
                                </div>
															</div>

															<div class = "row">
                                <div class = "col-sm-4">
																	<p class = "member-info member-table">Any open AMA member</p>
                                </div>
                                <div class = "col-sm-4">
																	<p class = "member-info member-table">Full time college undergrad/school student of any capability</p>
                                </div>
                                <div class = "col-sm-4">
																	<p class = "member-info member-table">Includes spouse and all in household 18 & under</p>
                                </div>
															</div>
                            </div>


													<p class = "border-line"></p>

                                                    <p>&nbsp;</p>

													<h4>These dues are additional to Academy of Model Aeronautics membership</h4>



													<p>AMA membership can be aquired from the <a class = "ama-button" href = "https://www.modelaircraft.org/joinrenew.aspx" target="_blank"><strong>AMA website</strong></a></p>

                                                    <p>&nbsp;</p>

													<h4>Central Iowa Aeromodeler membership requires proof of current Academy of Model Aeronautics membership.</h4>



													<h3><strong>No Refunds On Dues Paid</strong></h3>
  </div>
</div>
