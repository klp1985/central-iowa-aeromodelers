<?php

/**
 * @file
 * Bartik's theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>

<div class = "learn-container">
    <h2>Instruction is Free!!</h2>

    <div class = "row">
        <div class = "col-sm-7">
            <p>If you have an interest in learning to fly radio controled aircraft, we are here  to help you!! We have several members who are qualified to help you learn.  The CIA has several trainer type models, as well as some of the instructors have their own equipment. We use a instructor- student method where there are two transmitters connected with a cable whereby the instructor can give control of the aircraft to the student....and also take it back if necessary. Some of our instructors have over 40 years experience flying RC aircraft.</p>


            <h3>Consistent practice is the key to success.</h3>

            <p>Thursday evenings are set aside for training new pilots. It is the responsibility of the student to schedule with an instructor. (Weekends are available)</p>
        </div>

        <div class = "col-sm-5">
            <img src = "/section/work/ia_aero_mod/drupal/<?php print path_to_theme(); ?>/img/fly1.jpg" alt = "Model airplane flying" />
        </div>

<p>&nbsp;</p>

</div> <!-- End row -->

							<div class = "row instruct-pic">

								<h2 style = "border-bottom: 5px solid #820000; text-align: left;">Instructors:</h2>

                                    <p>&nbsp;</p>

									<div class = "col-sm-2">
											<img src = "/section/work/ia_aero_mod/drupal/<?php print path_to_theme(); ?>/img/2777200.jpg" alt = "Dave Sult" />
									</div>

									<div class = "col-sm-10">
									<h3 class = "underline">Dave Sult</h3>
									<p>Dave Sult has been flying R/C planes for 20 years. He is also an accomplished helicopter pilot. Dave will help you learn the fundamentals, and have you able to solo in no time. Dave can be reached at 387-1129. </p>
									</div>
							</div> <!-- End row -->

              <p style = "border-bottom: 2px solid #820000;"></p>

							<div class = "row instruct-pic">
									<div class = "col-sm-2">
											<img src = "/section/work/ia_aero_mod/drupal/<?php print path_to_theme(); ?>/img/2394141.jpg" alt = "Denny Goodrich" />
									</div>


									<div class = "col-sm-10">
									<h3 class = "underline">Denny Goodrich</h3>
									<p>Denny Goodrich has been flying R/C aircraft for more than 30 years. Denny's passion is to help a new student learn the fundamentals of controlling the aircraft to obtain the ultimate goal of solo flight. Phone Denny at 292-6326</p>
									</div>
							</div> <!-- End row -->

              <p style = "border-bottom: 2px solid #820000;"></p>

							<div class = "row instruct-pic">
									<div class = "col-sm-2">
											<img src = "/section/work/ia_aero_mod/drupal/<?php print path_to_theme(); ?>/img/6411506.jpg" alt = "Kent Woods" />
									</div>

									<div class = "col-sm-10">
									<h3 class = "underline">Kent Woods</h3>
									<p>Kent Woods has been flying R/C aircraft for over 45 years. He still enjoys the time spent helping a new pilot obtain the skills necessary to pilot model aircraft. Again, the ultimate goal is to solo. Kent sometimes uses sailplanes for dual instruction. Reach him at 232-8180</p>
									</div>
							</div> <!-- End row -->

<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>>
      <a href="<?php print $node_url; ?>"><?php print $title; ?></a>
    </h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php if ($display_submitted): ?>
    <div class="meta submitted">
      <?php print $user_picture; ?>
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <div class="content clearfix"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      print render($content);
    ?>
  </div>

  <?php
    // Remove the "Add new comment" link on the teaser page or if the comment
    // form is being displayed on the same page.
    if ($teaser || !empty($content['comments']['comment_form'])) {
      unset($content['links']['comment']['#links']['comment-add']);
    }
    // Only display the wrapper div if there are links.
    $links = render($content['links']);
    if ($links):
  ?>
    <div class="link-wrapper">
      <?php print $links; ?>
    </div>
  <?php endif; ?>

  <?php print render($content['comments']); ?>

</div>
</div>
