<?php

/**
 * @file
 * Bartik's theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>
<div class = "row">
    <div class = "col-sm-6">
        <div class = "map-container">
            <p><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3194.5650456661124!2d-93.540497!3d41.926128!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x98cf5de724fff861!2sCentral+Iowa+Aeromodelers!5e1!3m2!1sen!2sus!4v1492459679272" frameborder="0" style="border:0" allowfullscreen></iframe></p>
        </div>
    </div>
    <div class = "col-sm-6">
        <p>
        <img class = "runway-img" src = "/section/work/ia_aero_mod/drupal/<?php print path_to_theme(); ?>/img/runway.jpg" alt = "Image of airfield runway"/>
        </p>
    </div>
</div>

<p>&nbsp;</p>

<div class = "row">
		<p class = "field-info">Our Field is located East of Ames Iowa. From the junction of I35 and Hwy. 30 go about 4 miles East on Hwy. 30 then turn South on county road R70 (also called 580th Ave.) for about 5.5 miles, or about 2.5 miles north on R70 from the town of Cambridge. We have a 45X350 foot petromat runway and a grass strip also. The runway lays North South with clear airspace to the North, South and East. </p>

    <p>&nbsp;</p>

		<h2 style = "background-color: #f4f6f7; color: #164787; border: 2px ridge #092b60; width: 50%; margin: 0 auto; text-align: center;">You must belong to the <a href = "https://www.modelaircraft.org/joinrenew.aspx" style = "color: #820000;" target="_blank">AMA</a> to fly at our field</h2>

</div> <!-- End row -->

<p style = "border-bottom: 5px solid #820000;">&nbsp;</p>

<div class = "rulesHeading">
		<h1>Flying Field Rules</h1>

		<h2>Adherence to the AMA Safety Code is mandatory</h2>
		<h4>Updated 5-01-2009</h4>
</div><!-- End rulesHeading -->

<div class = "row" style = "margin: 2% 0">
		<div class = "col-sm-12 col-md-6">

				<h2>Frequency Control</h2>

				<ul>
						<li>Place pin on your AMA or club membership card onto the frequency board.  This includes 2.4.</li>

						<li>Be sure your channel is clear <strong>BEFORE</strong> turning your transmitter on.</li>

						<li>Remove pin from the board when done in order to share frequency.</li>
				</ul>

      </div> <!-- End col-sm-12 col-md-6 -->

									<div class = "col-sm-12 col-md-6">

										<h2>Flight Line Procedures</h2>

												<ul>

														<li>Start engines on stand or in runway entry.</li>

														<li>Point plaines toward the field, away from the pits.</li>

														<li>Restrain aircraft while starting and tuning engine.</li>

														<li>Pilots need to stand in alternating spaces.</li>

												</ul>

									</div> <!-- End col-sm-12 col-md-6 -->

								</div> <!-- End row -->

								<div class = "row" style = "margin: 2% 0">

									<div class = "col-sm-12 col-md-6">

										<h2>Pit Procedures</h2>

												<ul>

														<li>No gas or electric engine operations in pits.</li>

														<li>Maintenance and flying only with a cleared channel.</li>

														<li>Your card must be on frequency board for item 2.</li>

														<li>Fire extinguisher must be at hand for gasoline engines per <em><strong>AMA Safety Code</strong></em>.</li>

												</ul>

									</div> <!-- End col-sm-12 col-md-6 -->

									<div class = "col-sm-12 col-md-6">

										<h2>Noise Regulations</h2>

												<ul>

														<li>Mufflers are required on gas and nitro engines.</li>

														<li>Be respectful of others when tuning engine (example:  pointng tuned pipe exhausts away.)</li>

														<li>All AMA rules apply.</li>

														<li>Engine brake-in should be downwind.</li>

												</ul>

									</div> <!-- End col-sm-12 col-md-6 -->

								</div> <!-- End row -->


								<div class = "row" style = "margin: 2% 0">

									<div class = "col-sm-12 col-md-6">

										<h2>Airspace Rules</h2>

												<ul>

														<li>Wind indicator governs take-off and pattern direction.</li>

														<li>Do not fly behind the flight line.</li>

														<li>Keep all flying and landing at least 25 feet away from pilot.</li>

														<li>Use a spotter whenever possible.</li>

														<li>Alway make our first turn away from flight line.</li>

														<li>Share airspace and time equally with others, regardless of aircraft type.</li>

												</ul>

										</div> <!-- End col-sm-12 col-md-6 -->

									<div class = "col-sm-12 col-md-6">

										<h2>Right of Way</h2>

												<ul>

														<li>Emergency or deadstick landings have right of way.</li>

														<li>Pilots should announce intentions, such as "go around", "landing", etc., and land only if area is clear.</li>

														<li>People are allowed on the runway for placing or retrieving an aircraft <strong>only</strong> after yelling <strong>"On the Field!"</strong></li>

														<li>If you hear "On the Field", maintain higher altitude.</li>

												</ul>

									</div> <!-- End col-sm-12 col-md-6 -->

								</div> <!-- End row -->

								<div class = "row" style = "margin: 2% 0">

									<div class = "col-sm-12 col-md-6">

										<h2>Field Rules</h2>

												<ul>

														<li>Safegaurd our landlord's property.</li>

														<li>Respect crops when retrieving downed aircraft.</li>

														<li>No alcoholic beverages allowed.</li>

														<li>Carry equipment in if road or sod is soft or soggy.</li>

														<li>Avoid flying over highway.</li>

														<li>Clean up after yourself (cans, rubber bands, cigarette butts, etc.)</li>

														<li>For safety reasons, animals are to be leased and children supervised.</li>

												</ul>

									</div> <!-- End col-sm-12 col-md-6 -->

								</div> <!-- End row -->



<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>>
      <a href="<?php print $node_url; ?>"><?php print $title; ?></a>
    </h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php if ($display_submitted): ?>
    <div class="meta submitted">
      <?php print $user_picture; ?>
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <div class="content clearfix"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      print render($content);
    ?>
  </div>

  <?php
    // Remove the "Add new comment" link on the teaser page or if the comment
    // form is being displayed on the same page.
    if ($teaser || !empty($content['comments']['comment_form'])) {
      unset($content['links']['comment']['#links']['comment-add']);
    }
    // Only display the wrapper div if there are links.
    $links = render($content['links']);
    if ($links):
  ?>
    <div class="link-wrapper">
      <?php print $links; ?>
    </div>
  <?php endif; ?>

  <?php print render($content['comments']); ?>

</div>
