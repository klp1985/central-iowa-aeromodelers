<?php

/**
 * @file
 * Default theme implementation to present user list.
 */
?>
<?php global $base_url;?>
<?php if (isset($users)):?>
  <ul>
    <?php foreach ($users as $val):?>
      <?php $full_name = (isset($val->real_name) && !empty($val->real_name)) ? $val->real_name : $val->name;?>
      <?php $chat_name = (strlen($full_name) > 16) ? str_pad(drupal_substr($full_name, 0, 16), 18, '..') : $full_name;?>
      <?php
      $pic = '<img src="'. $base_url . '/' . drupal_get_path('module', 'googlechat') . '/images/default_avatar.png" />';
      if(!empty($val->uri))
      $pic = theme(
        'image_style', // The magic function image_style() for more detail you can check this link https://api.drupal.org/theme_image_style
        array(
          'style_name' => 'thumbnail', // You can choose your own style here from admin/config/media/image-styles
          'path' => $val->uri,
          'attributes' => array(
            'style' => 'width:20px;' // Your custom class for the  tag can be defined here.
          )
        )
      );
    ?>
      <li onclick="javascript:chatWith('<?php print $val->name; ?>')">
        <img id="<?php print $val->uid;?>_indicator" class="googlechatstatus googlechatstatus_<?php print $val->status_indicator;?>" src="<?php echo base_path() . drupal_get_path('module', 'googlechat');?>/images/cleardot.gif" alt="Online">
        <a id="googlechatbuddy_<?php echo str_replace(array('.', ' ', '@'), array('_', '_', '_'), $val->name); ?>" href="javascript:void(0)"  title="Chat with <?php print $full_name; ?>" name="<?php echo $chat_name; ?>">
        <?php echo $pic;?>&nbsp;&nbsp;<?php echo $chat_name; ?></a>
      </li>
    <?php endforeach;?>
  </ul>
<?php endif;?>
